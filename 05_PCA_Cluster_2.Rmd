---
title: 'Week 5: PCA, Cluster analysis 2'
output:
  html_document:
    df_print: paged
    toc: true
    toc_float: true
  html_notebook: default
  pdf_document: default
---

# Workshop
## Intro
Hierarchical clustering using Furthest Neighbour
```{r}
d <- matrix(c(
  0, NA, NA, NA, NA,
  2, 0, NA, NA, NA,
  6, 5 , 0, NA, NA,
  10, 9, 4, 0, NA,
  9, 8, 5, 3, 0
), 5,5, byrow = T)

rownames(d) <- c("A", "B", "C", "D", "E")
colnames(d) <- rownames(d)
d

distmat <- as.dist(d)
distmat
example <- hclust(distmat, method = "complete")
plot(as.dendrogram(example))
```

Hierarchical clustering using AVG distance
```{r}
example <- hclust(distmat, method = "average")
plot(as.dendrogram(example))
```

The order is the same but the distances are different.

## Question 1
Euclid distance, furthest neighbour.
```{r}
d <- matrix(c(
  0, NA, NA, NA, NA,
  4, 0, NA, NA, NA,
  2, 5 , 0, NA, NA,
  3, 4, 6, 0, NA,
  9, 8, 7, 7, 0
), 5,5, byrow = T)

rownames(d) <- c("A", "B", "C", "D", "E")
colnames(d) <- rownames(d)
# d

distmat <- as.dist(d)
example <- hclust(distmat, method = "complete")
plot(as.dendrogram(example))
```

## Question 2
Euclidean distance, furthest neighbour
```{r}
x1 <- c(9,3,7,7,2)
x2 <- c(5,2,6,1,1)
dat <- cbind(x1,x2)
rownames(dat) <- c("A","B","C","D", "E")
distmat <- dist(dat, method = "euclidean")
distmat

example <- hclust(distmat, method = "complete")
plot(as.dendrogram(example))
```

Now scale the data first to obtain Pearson distances. Furthest neighbour.
```{r}
dat.scale <- scale(dat)
distmat <- dist(dat.scale, method = "euclidean")
distmat

example <- hclust(distmat, method = "complete")
plot(as.dendrogram(example))
```

## Question 3
Load the data.
```{r}
library(readr)
Euroemp.all <- read_csv("data/Euroemp.csv")
Euroemp <- Euroemp.all[-1]
rownames(Euroemp) <- t(Euroemp.all[,1])
```
Euclidean distance furthest neighbour.
```{r}
distmat <- dist(Euroemp, method = "euclidean")
example <- hclust(distmat, method = "complete")
par(mar=c(5,4,4,7) + 0.1)
plot(as.dendrogram(example), horiz = T)
```

Pearson distance furthest neighbour
```{r}
Euroemp.scale <- scale(Euroemp)
distmat <- dist(Euroemp.scale, method = "euclidean")
example <- hclust(distmat, method = "complete")
par(mar=c(5,4,4,7) + 0.1)
plot(as.dendrogram(example), horiz = T)
```

## Question 4
```{r}
library(readr)
goblets <- read_csv("data/goblets.csv")
```

Euclidean distance cluster average
```{r}
distmat <- dist(goblets, method = "euclidean")
example <- hclust(distmat, method = "average")
plot(as.dendrogram(example))
```

Pearson distance cluster average
```{r}
goblets <- scale(goblets)
distmat <- dist(goblets, method = "euclidean")
example <- hclust(distmat, method = "average")
plot(as.dendrogram(example))
```

